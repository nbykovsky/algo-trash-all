### Preparation topics
#### Data Structures
* Linked list
* Queue
* Stacks
* Priority Queue, Heap
* Disjoint Set
* Trees
     * Balanced Trees
     * Red-Black Trees
     * Splay Trees
* Hash Tables
* Set
* Array, Dynamic Array
* Graphs
    * BFS
    * DFS
    * Dijkstra
    * Topological Sort
 
     
#### Algorithms
* Sorting
* Dynamic programming
* Bitwise operations
* Regex